/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author rafael
 */
public class EscolaN {
    String cod_escola;
    String cnpj_escola;
    String nome_fantasia_escola;

    public EscolaN(String cod, String cnpf, String nome) {
        this.cod_escola = cod;
        this.cnpj_escola = cnpf;
        this.nome_fantasia_escola = nome;
    }

    public String getCod_escola() {
        return cod_escola;
    }

    public void setCod_escola(String cod_escola) {
        this.cod_escola = cod_escola;
    }

    public String getCnpj_escola() {
        return cnpj_escola;
    }

    public void setCnpj_escola(String cnpj_escola) {
        this.cnpj_escola = cnpj_escola;
    }

    public String getNome_fantasia_escola() {
        return nome_fantasia_escola;
    }

    public void setNome_fantasia_escola(String nome_fantasia_escola) {
        this.nome_fantasia_escola = nome_fantasia_escola;
    }

   

    
  
}
