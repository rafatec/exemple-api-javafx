/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author rafael
 */
public class Escola {
    
    SimpleStringProperty cod;
    SimpleStringProperty cnpf;
    SimpleStringProperty nome;

    public Escola(String cod, String cnpf, String nome) {
        this.cod = new SimpleStringProperty(cod);
        this.cnpf = new SimpleStringProperty(cnpf);
        this.nome = new SimpleStringProperty(nome);
    }
    

    public String getCod() {
        return cod.get();
    }

    public void setCod(String cod) {
        this.cod.set(cod);
    }

    public String getCnpf() {
        return cnpf.get();
    }

    public void setCnpf(String cnpf) {
        this.cnpf.set(cnpf);
    }

    public String getNome() {
        return nome.get();
    }

    public void setNome(String nome) {
        this.nome.set(nome);
    }
    
    
    
}
