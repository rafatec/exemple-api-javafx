/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Conexao;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import javax.net.ssl.HttpsURLConnection;


/**
 *
 * @author rafael
 */
public class Request {
    
    public static String get(String urlString){
        String responsestring = "";
        try {
            URL url = new URL(urlString);
        HttpURLConnection c = (HttpURLConnection)url.openConnection();  //connecting to url
        c.setRequestMethod("GET");
        BufferedReader in = new BufferedReader(new InputStreamReader(c.getInputStream()));  //stream to resource
        String str;
        while ((str = in.readLine()) != null)   //reading data
           responsestring += str+"\n";//process the response and save it in some string or so
        in.close();
        } catch (Exception e) {
        }
        System.out.println(responsestring);
        return responsestring;     
    }
    
    public static String port(String urlString){
        String resp =  "";
        try {
            URL obj = new URL(urlString);
            HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();

            con.setRequestMethod("POST");

            String urlParameters = "'foo': 'bar','ca$libri': 'no$libri'";

            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(urlParameters);
            wr.flush();
            wr.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer res = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                res.append(inputLine);
            }
            in.close();
        
         } catch (Exception e) {
             
             
        }
        
        return  resp;
    }
   
    
}
