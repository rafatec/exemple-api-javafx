/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testeapimktech;

import Conexao.Request;
import com.google.gson.Gson;
import com.sun.javafx.collections.ElementObservableListDecorator;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import model.Escola;
import model.EscolaN;

/**
 *
 * @author rafael
 */
public class FXMLDocumentController extends Application implements Initializable {
    
    @FXML
    private TableView table_escolas;
    private String res = "";
    private ObservableList<Escola> escolas ;
    private List<Escola> list = new ArrayList<>();
     private EscolaN[] list2 ;
     private static Stage stage;
    
    @FXML 
    TableColumn cod_escolas;
    @FXML 
    TableColumn cnpj_escolas;
    @FXML 
    TableColumn nome_escolas;
    
    @FXML 
    ProgressIndicator progress_escola;
    
     Gson gson = new Gson();
    
    @FXML
    private void handleButtonAction(ActionEvent event) {
         new Thread(){
            public void run(){
                progress_escola.setVisible(true);
                res = Request.get("http://agenciamk3.com.br/api/escolas?chave=mktech2016");
                 
                list2 = gson.fromJson(res, EscolaN[].class);
                
                initItensTable(list2);
                 
            }

            
        }.start();
          
        
        
    }
    
    @FXML
    private void handleButtonTela2(ActionEvent event){
        
       ((Node)event.getSource()).getScene().getWindow().hide();
              Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("FXMLSecond.fxml"));
        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        Scene scene = new Scene(root);
       
        Stage stage = new Stage();
        stage.setScene(scene);
        stage.show();
        
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // INICIALIZA CELULAS DA TABELA
        cod_escolas.setCellValueFactory(new PropertyValueFactory<Escola,String>("cod"));
        cnpj_escolas.setCellValueFactory(new PropertyValueFactory<Escola,String>("cnpf"));
        nome_escolas.setCellValueFactory(new PropertyValueFactory<Escola,String>("nome"));
     
    }    

    @Override
    public void start(Stage primaryStage) throws Exception {
       FXMLDocumentController.stage = stage;
       
    }
    
     private void initItensTable(EscolaN[] list2) {
         
         list.clear();
         for(EscolaN e : list2){
             list.add(new Escola(e.getCod_escola(), e.getCnpj_escola(), e.getNome_fantasia_escola()));
         }
         
         table_escolas.getItems().clear();
         escolas = FXCollections.observableArrayList(list);
         table_escolas.setItems(escolas);
         progress_escola.setVisible(false);
       }
     public static Stage getStage() {
      return stage;
     }
     
    
}
